
# python core - commands which are accessible without any effort
# input for range ....

# python lib which requires import
# reside witihin our python library
import random

# in the future ...
# python lib which require downloading from the internet
# pip

#x = random.randint(0, 10) + 1 # 0-9 + 1 --> 1-10
#print(x)

print(random.uniform(1, 2))

my_score = 0
comp_score = 0
# foreach, for, while/do-while
while not (my_score >= 3 or comp_score >= 3):
    print('================================================')
    print(f"My score : {my_score} Computer score : {comp_score}")
    #input("Ready?")
    my_number = random.randint(1, 6)
    print(f"throwing my dice. number = {my_number}")

    comp_number = random.randint(1, 6)
    print(f"throwing computer dice. number = {comp_number}")

    if my_number > comp_number:
        my_score += 1
        print(f"I won ! {my_number} > {comp_number}")
    elif comp_number > my_number:
        comp_score += 1
        print(f"Computer won boooz ! {comp_number} > {my_number}")
    else:
        print(f"Draw ! {comp_number} == {my_number}")

print('================================================')
print(f"My score : {my_score} Computer score : {comp_score}")
if my_score == 3:
    print("I won !!!!")
else:
    print("Computer won booooz")

'''
0, 1, 1, 1, 0
0, 0, 0, 0, 0
0, 1, 1, 0, 0
1, 0, 0, 0, 0

'''

# computer will random a number between 1-100
# in loop
# user will guess the number
# if the user guessed higher then print "guess lower"
# if the user guessed lower then print "guess higher"
# if user guessed correctly then exit loop
# print how many attempts
counter = 0
secret_number = random.randint(1, 100)
while True:
    counter += 1
    guess = int(input("enter number (1-100):"))
    if guess == secret_number:
        break
    if guess > secret_number:
        print("you guessed to high")
    else:
        print("you guessed to low")

print(f"Bingo ! it took you {counter} guessed")




