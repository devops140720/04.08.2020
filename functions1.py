
# functions , why ?
# take some code and put it into a "box" == function
# so i wont have to write it twice (or more)
# python is based on functions
# i can write functions myself and expose them to user users
import random



def f1():
    pass

# my program
def main():
    bingo_game()
    print("done?")



# function bingo_game decleration
def bingo_game():
    secret_number = random.randint(1, 100)
    print(f"shhhhh {secret_number}")
    counter = 0
    while True:
        counter += 1
        guess = int(input("enter number (1-100):"))
        if guess == secret_number:
            break
        if guess > secret_number:
            print("you guessed to high")
        else:
            print("you guessed to low")

    print(f"Bingo ! it took you {counter} guessed")

main()